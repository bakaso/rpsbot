from aiogram import executor

from rpsbot.instance import bot, dp
from rpsbot.scenes import enable_scenes


if __name__ == "__main__":
    enable_scenes()
    executor.start_polling(dp, skip_updates=True)