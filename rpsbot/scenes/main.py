from aiogram import types

from rpsbot.instance import dp, bot
from rpsbot.keyboards import create_game_markup, signs, paginate_ultimate_markup, create_game_preparing_markup


@dp.message_handler(commands=['rps'], state="*")
async def select_variant(message):
    await message.reply(reply=False, text="Select variant:\n\n/rps_classic - Classic game\n/rps_rpssl - Rock Paper Scissors Spock Lizard\n/rps_extended = Extended 15-signs game\n/rps_maxi - 25-signs game\n/rps_ultimate - Ultimate 101-sign game")


@dp.inline_handler(state="*")
async def inline_mode(query, state):
    reply = [
        types.InlineQueryResultArticle(
            id="classic", title="Classic game", description="",
            input_message_content=types.InputTextMessageContent("Начинайте игру!"),
            reply_markup=create_game_preparing_markup("classic")
        ),
        types.InlineQueryResultArticle(
            id="rpssl", title="Rock-paper-scissors-spock-lizard", description="",
            input_message_content=types.InputTextMessageContent("Начинайте игру!"),
            reply_markup=create_game_preparing_markup("rpssl")
        ),
        types.InlineQueryResultArticle(
            id="extended", title="Extended 15-signs game", description="",
            input_message_content=types.InputTextMessageContent("Начинайте игру!"),
            reply_markup=create_game_preparing_markup("extended")
        ),
        types.InlineQueryResultArticle(
            id="maxi", title="25-signs game", description="",
            input_message_content=types.InputTextMessageContent("Начинайте игру!"),
            reply_markup=create_game_preparing_markup("maxi")
        ),
        types.InlineQueryResultArticle(
            id="ultimate", title="Ultimate 10-signs game", description="",
            input_message_content=types.InputTextMessageContent("Начинайте игру!"),
            reply_markup=create_game_preparing_markup("ultimate")
        ),
    ]
    await query.answer(results=reply, cache_time=1)


@dp.message_handler(lambda msg: msg.text.startswith("/rps_"), state="*")
async def start_game(message, state):
    state = dp.current_state(chat=message.chat.id, user=message.chat.id)
    variant = message.text.split("@")[0].split("_")[-1]
    msg = await message.reply("Начинайте игру!", reply_markup=create_game_markup(variant))
    async with state.proxy() as data:
        if msg.message_id not in data:
            data[msg.message_id] = {}
        data[msg.message_id] = {"players": [], "variant": variant}


@dp.callback_query_handler(lambda query: query.data.startswith("start_"), state="*")
async def start_inline_game(query, state):
    identifier = query.inline_message_id
    variant = query.data.split("_")[1]
    state = dp.current_state(chat=identifier, user=identifier)
    async with state.proxy() as data:
        data[query.inline_message_id] = {"players": [], "variant": variant}
    await bot.edit_message_reply_markup(inline_message_id=identifier, reply_markup=create_game_markup(variant))


@dp.callback_query_handler(lambda query: query.data.startswith("page_"), state="*")
async def paginate_ultimate_game(query, state):
    page = int(query.data.split("_")[1])
    kb = paginate_ultimate_markup(page)
    if query.message is None:
        await bot.edit_message_reply_markup(inline_message_id=query.inline_message_id, reply_markup=kb)
    else:
        await query.message.edit_reply_markup(kb)
    await query.answer()


def sort_signs(signs, current):
    position = signs.index(current)
    left_part = signs[:position]
    right_part = signs[position+1:]
    ordered = right_part + left_part
    return ordered[:int(len(ordered)/2)]


def check_winner(variant, first, second):
    signs_set = sort_signs(signs[variant], first)
    if second in signs_set:
        return True
    return False


@dp.callback_query_handler(lambda call: True, state="*")
async def turn(call, state):
    if call.message is not None:
        identifier = call.message.chat.id
        inline = False
        path = call.message.message_id
    else:
        identifier = call.inline_message_id
        inline = True
        path = identifier
    state = dp.current_state(chat=identifier, user=identifier)
    async with state.proxy() as data:
        variant = data[path]["variant"]
        if len(data[path]["players"]) == 0:
            data[path]["players"].append((call.from_user.first_name, call.data, call.from_user.id))
            if inline:
                await bot.edit_message_text(inline_message_id=identifier, text=f"{call.from_user.first_name} сделал ход!", reply_markup=create_game_markup(variant))
            else:
                await call.message.edit_text(f"{call.from_user.first_name} сделал ход!", reply_markup=create_game_markup(variant))
            return await call.answer("Дождитесь хода второго игрока!")
        if data[path]["players"][0][2] == call.from_user.id:
            return await call.answer("Вы уже сходили!")
        winner = data[path]["players"][0][0] if check_winner(variant, data[path]["players"][0][1], call.data) else call.from_user.first_name
        if data[path]["players"][0][1] == call.data:
            winner = "Ничья"
        if inline:
            await bot.edit_message_text(inline_message_id=identifier, text=f"{data[path]['players'][0][0]}: {data[path]['players'][0][1]}\n{call.from_user.first_name}: {call.data}\nПобедитель: {winner}")
        else:
            await call.message.edit_text(text=f"{data[path]['players'][0][0]}: {data[path]['players'][0][1]}\n{call.from_user.first_name}: {call.data}\nПобедитель: {winner}")
        del data[path]
        await call.answer()
