from aiogram import types

from rpsbot import emoji as e


signs = {
    "classic": [e.ROCK, e.SCISSORS, e.PAPER],
    "rpssl": [
        e.ROCK, e.SCISSORS, e.LIZARD, e.PAPER, e.SPOCK
    ],
    "extended": [
        e.ROCK,
        e.FIRE,
        e.SCISSORS,
        e.SNAKE,
        e.MAN,
        e.TREE,
        e.WOLF,
        e.SPONGE,
        e.PAPER,
        e.AIR,
        e.WATER,
        e.DRAGON,
        e.DEVIL,
        e.LIGHTNING,
        e.GUN
    ],
    "maxi": [
        e.ROCK,
        e.SUN,
        e.FIRE,
        e.SCISSORS,
        e.AXE,
        e.SNAKE,
        e.MONKEY,
        e.WOMAN,
        e.MAN,
        e.TREE,
        e.COCKROACH,
        e.WOLF,
        e.SPONGE,
        e.PAPER,
        e.MOON,
        e.AIR,
        e.BOWL,
        e.WATER,
        e.ALIEN,
        e.DRAGON,
        e.DEVIL,
        e.LIGHTNING,
        e.NUKE,
        e.DYNAMITE,
        e.GUN
    ],
    "ultimate": [
        e.ROCK,
        e.DEATH,
        e.WALL,
        e.SUN,
        e.CAMERA,
        e.FIRE,
        e.CHAINSAW,
        e.SCHOOL,
        e.SCISSORS,
        e.POISON,
        e.CAGE,
        e.AXE,
        e.PEACE,
        e.COMPUTER,
        e.CASTLE,
        e.SNAKE,
        e.BLOOD,
        e.PORCUPINE,
        e.VULTURE,
        e.MONKEY,
        e.KING,
        e.QUEEN,
        e.PRINCE,
        e.PRINCESS,
        e.POLICE,
        e.WOMAN,
        e.BABY,
        e.MAN,
        e.HOME,
        e.TRAIN,
        e.CAR,
        e.NOISE,
        e.BICYCLE,
        e.TREE,
        e.TURNIP,
        e.DUCK,
        e.WOLF,
        e.CAT,
        e.BIRD,
        e.FISH,
        e.SPIDER,
        e.COCKROACH,
        e.BRAIN,
        e.COMMUNITY,
        e.CROSS,
        e.MONEY,
        e.VAMPIRE,
        e.SPONGE,
        e.CHURCH,
        e.BUTTER,
        e.BOOK,
        e.PAPER,
        e.CLOUD,
        e.AIRPLANE,
        e.MOON,
        e.GRASS,
        e.FILM,
        e.TOILET,
        e.AIR,
        e.PLANET,
        e.GUITAR,
        e.BOWL,
        e.CUP,
        e.BEER,
        e.RAIN,
        e.WATER,
        e.TV,
        e.RAINBOW,
        e.UFO,
        e.ALIEN,
        e.PRAYER,
        e.MOUNTAIN,
        e.SATAN,
        e.DRAGON,
        e.DIAMOND,
        e.PLATINUM,
        e.GOLD,
        e.DEVIL,
        e.FENCE,
        e.VIDEOGAME,
        e.MATH,
        e.ROBOT,
        e.HEART,
        e.ELECTRICITY,
        e.LIGHTNING,
        e.MEDUSA,
        e.POWER,
        e.LASER,
        e.NUKE,
        e.SKY,
        e.TANK,
        e.HELICOPTER,
        e.DYNAMITE,
        e.TORNADO,
        e.QUICKSAND,
        e.PIT,
        e.CHAIN,
        e.GUN,
        e.LAW,
        e.WHIP,
        e.SWORD
    ]
}


def paginate_ultimate_markup(page):
    if page == 1:
        new_prev_page = 7
    else:
        new_prev_page = page - 1
    if page == 7:
        new_next_page = 1
    else:
        new_next_page = page + 1
    start_range = 14*(page-1)
    end_range = start_range + 14
    kb = types.InlineKeyboardMarkup(row_width=5)
    kb.insert(types.InlineKeyboardButton("?", url="https://www.umop.com/rps101.htm"))
    signs_set = list(filter(lambda x: x not in (e.ROCK, e.PAPER, e.SCISSORS), signs['ultimate']))
    for sign in signs_set[start_range:end_range]:
        kb.insert(types.InlineKeyboardButton(sign, callback_data=sign))
    kb.insert(types.InlineKeyboardButton("<", callback_data="page_"+str(new_prev_page)))
    for sign in (e.ROCK, e.PAPER, e.SCISSORS):
        kb.insert(types.InlineKeyboardButton(sign, callback_data=sign))
    kb.insert(types.InlineKeyboardButton(">", callback_data="page_"+str(new_next_page)))
    return kb


def create_game_markup(variant):
    if variant == "ultimate":
        kb = types.InlineKeyboardMarkup(row_width=5)
        kb.insert(types.InlineKeyboardButton("?", url="https://www.umop.com/rps101.htm"))
        signs_set = list(filter(lambda x: x not in (e.ROCK, e.PAPER, e.SCISSORS), signs[variant]))
        for sign in signs_set[:14]:
            kb.insert(types.InlineKeyboardButton(sign, callback_data=sign))
        kb.insert(types.InlineKeyboardButton("<", callback_data="page_7"))
        for sign in (e.ROCK, e.PAPER, e.SCISSORS):
            kb.insert(types.InlineKeyboardButton(sign, callback_data=sign))
        kb.insert(types.InlineKeyboardButton(">", callback_data="page_2"))
        return kb
    kb = types.InlineKeyboardMarkup(row_width=5)
    for sign in signs[variant]:
        kb.insert(types.InlineKeyboardButton(sign, callback_data=sign))
    return kb


def create_game_preparing_markup(variant):
    kb = types.InlineKeyboardMarkup()
    kb.row(types.InlineKeyboardButton("Начали!", callback_data="start_"+variant))
    return kb
